<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

define('DS', '/');
define('APP_PATH', __DIR__ . DS . '..' . DS . 'application' . DS);
set_include_path(APP_PATH); // adding new path to include_path
spl_autoload_extensions('.php'); // comma-separated list
spl_autoload_register();

// Here would be initilized application: read and store POST, COOKIES, GET
\Core\App::init();

// Than try to login (call new \Model\User\User())
\Core\Auth::init();

// Run router
\Core\Route::init(\Core\App::getURIRoutes());
?>