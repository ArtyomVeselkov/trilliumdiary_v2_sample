<?php

/**
 * @author phpdesigner
 * @copyright 2016
 */

namespace Common;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class ObjectProto 
{
    public static function instance() 
    {
        $name = get_called_class();
        $key  = '';
        if (method_exists($name, 'getStorageID'))
            try {
                $key = $name::getStorageID();
            } catch (\Exception $e) {
                $key = '';
            }
        $key = ('' == $key) ? $name : $key;
            
        return \Common\Storage::is($key) ?
            \Common\Storage::get($key) :                    
            \Common\Storage::set( 
                $key, 
                (0 < func_num_args()) ?
                    (new \ReflectionClass($name))->NewInstanceArgs(func_get_args()) :
                    new $name
            );  
    }
}
?>