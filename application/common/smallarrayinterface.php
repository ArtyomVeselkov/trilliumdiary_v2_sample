<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Common;

interface SmallArrayInterface 
{
    public static function set($key, $object);
    
    public static function get($key, $def = null);
    
    public static function del($key);
    
    public static function is($key);
}

?>