<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
namespace Common;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

trait SmallArrayTrait 
{
    private static
        $data = array();

    public static function set($key, $object) 
    {
        try {
            if (isset(static::$data[$key]) && ($object !== $data[$key])) {
                static::del($key);
            }
        } catch (exception $e) {
            return null;
        }
        return static::$data[$key] = $object;
    }
    
    public static function get($key, $def = null) 
    {
        return isset(static::$data[$key]) ? static::$data[$key] : $def;
    }
    
    public static function del($key) 
    {
        static::$data[$key] = null;
        unset(static::$data[$key] );
    }
    
    public static function is($key) 
    {
        return isset(static::$data[$key]);
    }
}

?>