<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
namespace Common;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

final class Storage implements \Common\SmallArrayInterface 
{
    use \Common\SmallArrayTrait;
    
    final function __clone() {}
    
    final function __construct() {}
}


?>