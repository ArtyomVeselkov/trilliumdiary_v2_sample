<?php

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

return [
    'db_name'       => 'mysql',
    'db_password'   => 'localhost',
    'db_user'       => 'jDiary',
    'db_engine'     => 'root',    
    'db_host'       => ''
];
?>