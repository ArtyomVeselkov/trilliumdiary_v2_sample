<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
namespace Controller;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class Index extends \Core\Controller
{  
    // false -- active rendering mode, true -- silent mode 
    private $mode = false; 
    
    function __construct($mode)
    {
        $this->$mode = $mode;
    }
    
    public function index()
    {
        if ($this->$mode)
            \Core\View::instance()->render('template.php');
    }
}

?>