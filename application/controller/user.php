<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Controller;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class User extends \Core\Controller

{
    public static function getStatus()
    {
        return \Model\User::getStatus();
    }
    
    function __construct()
    {
        $nick = \Core\App::getCookie('user_nick', null);
        $hash = \Core\App::getCookie('user_pwd', null);
        
        switch (static::getStatus($nick, $hash))
        {
            case 'guest':
                \Model\User::instance();
                break;
            case 'child':
                \Model\User\Child::instance();
                break;
            case 'mother':
                \Model\User\Mother::instance();
                break;
            case 'father':
                \Model\User\Father::instance();
                break;
        }
    }
}

?>