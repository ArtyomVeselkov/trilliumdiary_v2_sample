<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Core;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class App
{ 
     const DEF_COOKIES_PROLONGATION = 31104000;
     
    /**
     * All elements from $_SERVER
     * 
     * @var array
     */
    private static $server_common  = array();
    /**
     * Consists of URI splitted into components
     * 
     * @var array
     */
    private static $uri_routes     = array(); 
    
    private static $cookies        = array();
    
    public static function init()
    {
        static::$server_common = $_SERVER; 
        static::$uri_routes    = explode('/', static::$server_common['REQUEST_URI']);
        static::$cookies       = $_COOKIES;
    }
    
    public static function getURIRoutes()
    {
        return static::$uri_routes;
    } 
        
    public static function getHostURI()
    {
        return 'http://' . static::$server_common['HTTP_HOST'];
    }
    
    public static function sendHeader($header)
    {
        header($header);
    }
    
    public static function getCookie($key, $default = '')
    {
        return isset($cookies[$key]) ? $cookies[$key] : $default;
    }

    
}

?>