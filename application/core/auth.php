<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
    
namespace Core;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );
    
class Auth 
{
    public static function init()
    {
        // true -- indicate to do this in "silent mode"
        \Controller\User::instance(true);
    }
}
?>