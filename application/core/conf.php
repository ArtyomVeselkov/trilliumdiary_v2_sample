<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Core;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

final class Conf implements \Common\ArrayInterface 
{
    const
        DEF_CONF_FILE = '../conf.php';
    
    use JArrayTrait;    
    
    public static function load($file = '') {
        $file = (('' != $file) && (file_exists($file))) ? $file : BASEPATH . static::DEF_CONF_FILE;
        if (file_exists($file))
            static::$data = require( $file );
    }
    
    use \Common\JArrayTrait;
    
    final function __clone() {}
    
    final function __construct() {}
}


?>