<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Core;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );


class DBConnector extends \Common\ObjectProto
{
    private static $pdo  = null;
    private static $db_pwd  = '';
    private static $db_user = '';
    private static $db_eng  = '';
    private static $db_host = ''; 
    private static $db_port = '';
    private static $db_name = '';
    
    private static $result  = array();
    private static $query   = '';
    private static $last_id = '';
        
    private static function init() 
    {
        static::$db_name  = \Core\Conf::get('db_name', '');
        static::$db_pwd  = \Core\Conf::get('db_password');
        static::$db_user = \Core\Conf::get('db_user');
        static::$db_eng  = \Core\Conf::get('db_engine', 'mysql');
        static::$db_host = \Core\Conf::get('db_host', 'localhost');
        static::$db_port = \Core\Conf::get('db_port', '3306');
    }
    
    function __construct() 
    {
        if (!static::$pdo) {
        
            static::init();    
            
            try {
                $dsn = static::$db_eng . ':host=' . static::$db_host . '; port=' . static::$db_port 
                    . '; dbname=' . static::$db_name . '; charset=utf8';
                    
                static::$pdo = new PDO( $dsn, static::$db_user, static::$db_pwd );
                static::$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
                static::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                return null;
            }
        }
    }
    
    private static function clearResult() 
    {
        if (static::$result) {
            static::$result = null;
            unset(static::$result);        
        }
    }
    
    private static function setLastInsertID($reset = false) 
    {
        static::$last_id = ($reset ? null : static::$pdo->lastInsertId());           
    }
    
    public function getLastInsertID() 
    {
        return static::$last_id;           
    }
    
    private static function addQuery($query, $reset = false)
    {
        static::$query = $reset ? $query : static::$query . $query;
        if ('' != trim(static::$query))
            static::$query .= ";\n";
    }
    
    public function getQuery() 
    {
        return static::$query;
    }    
    
    public function getResult() 
    {
        return static::$result;
    }

    
    private function push($data = null) {
        if (!static::$pdo)
            return false;
            
        try {
            static::$pdo->beginTransaction();
            
            $sth = static::$pdo->prepare(static::$query);
            
            $sth->execute($data);
            $this->setLastInsertID();
            
            static::$pdo->commit();
            
            static::clearResult();
            static::$result = $sth->fetchAll(PDO::FETCH_ASSOC | PDO::FETCH_UNIQUE);            
        } catch (PDOException $e) {
            static::addQuery('', true);
            static::$pdo->rollback();
            static::setLastInsertID(true);
            return false;
        }
        static::addQuery('', true);
        return true;
    }
    
    protected static function getQueryResult()
    {
        return static::$result;
    } 
    
    public function selectAll($data) 
    {
        if ((!$data) || (!$this->isEnoughData($data,true)))
            return null;
        
        $this->filterDataComplex($data_f, $data);

        $s_keys = '`' . implode('`, `', array_keys($this->tf)) . '`';
        
        $s_stmt = implode(' AND ', array_map( 
            function($k, $v){ 
                return '`' . $this->tblname . '`.`' . $k . '` = ' . 
                      ':' . $k; },
                array_keys($data_f), $data_f) );
                
        $this->addQuery('SELECT ' . $s_keys .  ' FROM `' . $this->tblname . '` ' 
            . $s_join . "\n\tWHERE " . $s_stmt)->push($data_f);
    }
    
    protected static function select($select_cols, $table_name, $where)
    {
        $q  = 'SELECT `' . implode('`, `', $select_cols) . '`' . "\n\t";
        $q .= 'FROM `' . $table_name . '`' . "\n";
        $q  = 'WHERE ' . implode(' AND ', array_map( 
                function($k){ 
                    return $k . ' = ' . ':' . $k; 
                },
                array_keys($where)
            )
        );
        
        static::addQuery($q);
        return static::push($where);
    }
    
}
   
?>