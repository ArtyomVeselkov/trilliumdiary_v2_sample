<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */
 
if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class Route
{    
    public static function dispatch($uri_routs)
    {
        //! First component -- is a contoller name, second -- method of that controller        
        $controller_name   = (isset($uri_routs[0]) && (!in_array($uri_routs[0], array('', '/')))) ? 
            $uri_routs[0] : 'index';
        $controller_method = isset($uri_routs[1]) ? $uri_routs[1] : '';
        
        $class_ns_path     = '\\Contoller\\' . $controller_name;
        
        try {
            // not silent mode
            $contoller = $class_ns_path::instance(false);
            if (('' != $controller_method) && (method_exists($contoller, $controller_method)))
                $contoller->$controller_method;
        } catch (\Exception $e) {
            static::dispatchDefault(404);
        }
        
    }
    
    public function dispatchDefault($code = 303)
    {
        switch ($code)
        {
            case 303:
                // Make redirection
                // \Core\View::renderDefault('303');
                break;
            case 404:
                \Core\App::sendHeader('HTTP/1.1 404 Not Found');
                \Core\App::sendHeader('Status: 404 Not Found');
                //\Core\App::sendHeader('Location: ' . \Core\App::getHostURI() . '404');
                //\Core\View::renderDefault('404');
        }
    }
}

?>