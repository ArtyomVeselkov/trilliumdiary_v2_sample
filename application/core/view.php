<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Core;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class View extends \Common\ObjectProto 
{    
    private $blocks = array();
    
    public function append($template, $data)
    {
        $main_template_path = BASEPATH . '/core/view/' . $main_template;  
        if (file_exists($main_template_path))
            include $main_template_path; 
    }
    
    public function render($main_template)
    {
        $main_template_path = BASEPATH . '/core/view/' . $main_template;  
        if (file_exists($main_template_path))
            include $main_template_path; 
    }
}


?>