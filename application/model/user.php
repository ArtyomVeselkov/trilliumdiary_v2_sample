<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Model;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );
    
class User extends \Core\Model
{
    private static $tabel_name = 'users';
    private static $grants     = array(
        'view'      => false,
        'distibute' => false,
        'upload'    => false,
        'markdone'  => false
    );
    
    private static $status     = '';
    
    private static function SQLEnum2Status($status)
    {
        switch ($status) 
        {
            case 'M': return 'mother';
            case 'F': return 'father';
            case 'C': return 'child';
            default: return 'guest'; 
        }
    }
    
    public static function getStorageID()
    {
        return 'user';
    }
    
    private static function loadStatus($user_nick, $user_pwd, $user_id = null)
    {
        /* It looks like this
         *
         * SELECT 'user_status' 
         * FROM 'users' 
         * WERE user_nick = 'nick' AND user_pwd = 'pwd'; 
         */
        $ok = false;
        if (($user_nick) && ($user_pwd))
            $ok = static::select(
                array('user_status'),
                static::$tabel_name,
                array(
                    'user_id' => $user_id
                )
            );
        elseif (null !== $user_id)
            $ok = static::select(
                array('user_status'),
                static::$tabel_name,
                array(
                    'user_nick' => $user_nick,
                    'user_pwd' => $user_pwd,
                )
            );
        else
        static::$status = $ok ? static::SQLEnum2Status(static::getQueryResult()[0]) : 'guest';
    }
    
    public static function getStatus($user_nick = null, $user_pwd = null, $user_id = null)
    {
        if ('' == static::$status)
            static::loadStatus($user_nick, $user_pwd, $user_id);
        return static::$status;
    }
    
    public static function can($action)
    {
        return isset(static::$grants[$action]) ? static::$grants[$action] : false;
    }
    
    function __construct($mode)
    {
        
    }
}

?>