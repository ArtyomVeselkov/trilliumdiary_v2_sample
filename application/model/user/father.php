<?php

/**
 * @author ArtyomVeselkov
 * @copyright 2016
 */

namespace Model\User;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class Father extends \Model\User 
{
    private static $grants     = array(
        'view'      => true,
        'distibute' => true,
        'upload'    => false,
        'markdone'  => true
    );
} 

?>