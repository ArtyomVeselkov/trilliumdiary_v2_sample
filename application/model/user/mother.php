<?php

/**
 * @author phpdesigner
 * @copyright 2016
 */
 
namespace Model\User;

if ( !defined( '\BASEPATH' ) )
    die( 'Access denied' );

class Mother extends \Model\User 
{
    private static $grants     = array(
        'view'      => true,
        'distibute' => false,
        'upload'    => true,
        'markdone'  => true
    );
} 

?>